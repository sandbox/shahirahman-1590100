<?php

/**
 * Copyright 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package uc_charges
 * @author Horoppa Software
 * @link http://www.horoppa.com
 * @since Version 1.0
 */

/**
 * Implements hook_schema().
 */
function uc_charges_schema() {
   $schema = array();
      $schema['uc_charges_per_item'] = array(
        'description' => 'Stores charges per item.',
        'fields' => array(
          'mid' => array(
            'description' => 'Primary key: The shipping quote method ID.',
            'type' => 'serial',
            'unsigned' => TRUE,
            'not null' => TRUE,
          ),
          'title' => array(
            'description' => 'The method title, displayed on administration pages.',
            'type' => 'varchar',
            'length' => 255,
            'not null' => TRUE,
            'default' => '',
          ),
          'label' => array(
            'description' => 'The user-facing label of the shipping method.',
            'type' => 'varchar',
            'length' => 255,
            'not null' => TRUE,
            'default' => '',
          ),
          'base_rate' => array(
            'description' => 'The amount of shipping cost before product quantity is applied.',
            'type' => 'numeric',
            'precision' => 16,
            'scale' => 5,
            'not null' => TRUE,
            'default' => 0.0,
          ),
        ),
        'primary key' => array('mid'),
      );

  $schema['uc_charges_per_order'] = array(
    'description' => 'Stores charges per order.',
    'fields' => array(
      'mid' => array(
        'description' => 'Primary key: The shipping quote method ID.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'title' => array(
        'description' => 'The method title, displayed on administration pages.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'label' => array(
        'description' => 'The user-facing label of the shipping method.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'base_rate' => array(
        'description' => 'The amount of shipping cost before product quantity is applied.',
        'type' => 'numeric',
        'precision' => 16,
        'scale' => 5,
        'not null' => TRUE,
        'default' => 0.0,
      ),
      'active' => array(
        'description' => 'Boolean indicating whether the configuration is active. Usage depends on how the using module makes use of it.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 1,
    ),
    ),
    'primary key' => array('mid'),
  );
  return $schema;
}
