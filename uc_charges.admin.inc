<?php
/**
 * Copyright 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package uc_charges
 * @author Horoppa Software
 * @link http://www.horoppa.com
 * @since Version 1.0
 */

/**
 * Configures the store default product shipping rates.
 *
 * @see uc_per_item_charges_admin_method_edit_form_submit()
 * @see uc_per_item_charges_admin_method_edit_form_delete()
 * @ingroup forms
 */
function uc_charges_per_item_method_settings($form, &$form_state)
{
    foreach (uc_charges_per_item_methods(TRUE) as $method) {
        if (isset($method['a_charge'])) {
            $id = $method['id'];

            // Build a list of operations links.
            $operations = isset($method['operations']) ? $method['operations'] : array();

            // Ensure "delete" comes towards the end of the list.
            if (isset($operations['delete'])) {
                $operations['delete']['weight'] = 10;
            }
            uasort($operations, 'drupal_sort_weight');
            $form['methods'][$id]['uc_charges_per_item_enabled'] = array(
                '#type' => 'checkbox',
                '#title' => check_plain($method['title']),
                '#default_value' => $method['enabled'],
            );
            $form['methods'][$id]['description'] = array(
                '#markup' => isset($method['description']) ? $method['description'] : '',
            );
            $form['methods'][$id]['operations'] = array(
                '#theme' => 'links',
                '#links' => $operations,
                '#attributes' => array('class' => array('links', 'inline')),
            );
        }
    }

    return $form;
}

/**
 * Per-item admin editing form method
 *
 * @param $form
 * @param $form_state
 * @param int $mid
 * @return array
 */
function uc_charges_per_item_admin_method_edit_form($form, &$form_state, $mid = 0)
{
    if ($mid && ($method = db_query("SELECT * FROM {uc_charges_per_item} WHERE mid = :mid", array(':mid' => $mid))->fetchObject())) {
        $form['mid'] = array('#type' => 'value', '#value' => $mid);
    } else {
        $query = db_query("SELECT count(mid) FROM {uc_charges_per_item}")->fetchField();
        if ($query) {
            drupal_set_message('Only one per item charges can be set', 'error');
            drupal_goto('admin/store/settings/charges');
        }
        $method = (object)array(
            'title' => '',
            'label' => '',
            'base_rate' => '',
        );
    }
    $form['title'] = array(
        '#type' => 'textfield',
        '#title' => t('Per item charges title'),
        '#description' => t('The name shown to administrators'),
        '#default_value' => $method->title,
        '#required' => TRUE,
    );
    $form['label'] = array(
        '#type' => 'textfield',
        '#title' => t('Line item label'),
        '#description' => t('The name shown to the customer at checkout'),
        '#default_value' => $method->label,
        '#required' => TRUE,
    );
    $form['base_rate'] = array(
        '#type' => 'uc_price',
        '#title' => t('Per item charges'),
        '#description' => t('Amount of per item charges applied per order'),
        '#default_value' => $method->base_rate,
        '#required' => TRUE,
    );
    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Submit'),
    );
    if (isset($form['mid'])) {
        $form['actions']['delete'] = array(
            '#type' => 'submit',
            '#value' => t('Delete'),
            '#validate' => array(),
            '#submit' => array('uc_charges_per_item_admin_method_edit_form_delete'),
        );
    }

    return $form;
}

/**
 * Form submission handler for uc_charges_per_item_admin_method_edit_form().
 *
 * @see uc_charges_per_item_admin_method_edit_form()
 */
function uc_charges_per_item_admin_method_edit_form_submit($form, &$form_state)
{
    if (isset($form_state['values']['mid'])) {
        drupal_write_record('uc_charges_per_item', $form_state['values'], 'mid');
        drupal_set_message(t('Per item charges was updated.'));
        $form_state['redirect'] = 'admin/store/settings/charges/methods';
    } else {
        drupal_write_record('uc_charges_per_item', $form_state['values']);

        // Ensure Rules picks up the new condition.
        entity_flush_caches();
        drupal_set_message(t('Created and enabled new per item charges.'));
        $form_state['redirect'] = 'admin/store/settings/charges/manage/get_a_charges_from_charges_' . $form_state['values']['mid'];
    }
}

/**
 * Helper function to delete a per_item_charges method.
 *
 * @see uc_charges_per_item_admin_method_edit_form_delete()
 */
function uc_charges_per_item_admin_method_edit_form_delete($form, &$form_state)
{
    drupal_goto('admin/store/settings/charges/charges/' . $form_state['values']['mid'] . '/delete');
}

/**
 * Confirms deletion of Per Item Charges.
 *
 * @see uc_per_item_charges_admin_method_confirm_delete_submit()
 * @ingroup forms
 */
function uc_charges_per_item_admin_method_confirm_delete($form, &$form_state, $mid)
{
    $form['mid'] = array('#type' => 'value', '#value' => $mid);
    return confirm_form($form, t('Do you want to delete this per item charges?'),
        'admin/store/settings/charges/methods',
        t('This will remove the per item charges'),
        t('Delete'));
}

/**
 * Form submission handler for uc_per_item_charges_admin_method_confirm_delete().
 *
 * @see uc_per_item_charges_admin_method_confirm_delete()
 */
function uc_charges_per_item_admin_method_confirm_delete_submit($form, &$form_state)
{
    $mid = $form_state['values']['mid'];
    db_delete('uc_charges_per_item')
        ->condition('mid', $mid)
        ->execute();
    rules_config_delete(array('get_quote_from_per_item_charges_' . $mid));
    drupal_set_message(t('Per item charges deleted.'));
    $form_state['redirect'] = 'admin/store/settings/charges/methods';
}

/**
 * Configures the store default product shipping rates.
 *
 * @see uc_per__charges_admin_method_edit_form_submit()
 * @see uc_per_order_charges_admin_method_edit_form_delete()
 * @ingroup forms
 */
function uc_charges_per_order_method_settings($form, &$form_state)
{
    foreach (uc_charges_per_order_methods(TRUE) as $method) {
        if (isset($method['a_charge'])) {
            $id = $method['id'];

            // Build a list of operations links.
            $operations = isset($method['operations']) ? $method['operations'] : array();

            // Ensure "delete" comes towards the end of the list.
            if (isset($operations['delete'])) {
                $operations['delete']['weight'] = 10;
            }
            uasort($operations, 'drupal_sort_weight');
            $form['methods'][$id]['uc_charges_per_order_enabled'] = array(
                '#type' => 'checkbox',
                '#title' => check_plain($method['title']),
                '#default_value' => $method['enabled'],
            );
            $form['methods'][$id]['description'] = array(
                '#markup' => isset($method['description']) ? $method['description'] : '',
            );
            $form['methods'][$id]['operations'] = array(
                '#theme' => 'links',
                '#links' => $operations,
                '#attributes' => array('class' => array('links', 'inline')),
            );
        }
    }
    $form['actions'] = array('#type' => 'actions');

    return $form;
}

/**
 * Per-order admin editing form method
 *
 * @param $form
 * @param $form_state
 * @param int $mid
 * @return array
 */
function uc_charges_per_order_admin_method_edit_form($form, &$form_state, $mid = 0)
{
    if ($mid && ($method = db_query("SELECT * FROM {uc_charges_per_order} WHERE mid = :mid", array(':mid' => $mid))->fetchObject())) {
        $form['mid'] = array('#type' => 'value', '#value' => $mid);
    } else {
        $method = (object)array(
            'title' => '',
            'label' => '',
            'base_rate' => '',
        );
    }
    $form['title'] = array(
        '#type' => 'textfield',
        '#title' => t('Per order charges title'),
        '#description' => t('The name shown to administrators'),
        '#default_value' => $method->title,
        '#required' => TRUE,
    );
    $form['label'] = array(
        '#type' => 'textfield',
        '#title' => t('Line item label'),
        '#description' => t('The name shown to the customer at checkout'),
        '#default_value' => $method->label,
        '#required' => TRUE,
    );
    $form['base_rate'] = array(
        '#type' => 'uc_price',
        '#title' => t('Per order charges'),
        '#description' => t('Amount of per order charges applied per order'),
        '#default_value' => $method->base_rate,
        '#required' => TRUE,
    );
    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Submit'),
    );
    if (isset($form['mid'])) {
        $form['actions']['delete'] = array(
            '#type' => 'submit',
            '#value' => t('Delete'),
            '#validate' => array(),
            '#submit' => array('uc_charges_per_order_admin_method_edit_form_delete'),
        );
    }

    return $form;
}

/**
 * Form submission handler for uc_per_order_charges_admin_method_edit_form().
 *
 * @see uc_per_order_charges_admin_method_edit_form()
 */
function uc_charges_per_order_admin_method_edit_form_submit($form, &$form_state)
{
    if (isset($form_state['values']['mid'])) {
        drupal_write_record('uc_charges_per_order', $form_state['values'], 'mid');
        drupal_set_message(t('Per order charges was updated.'));
        $form_state['redirect'] = 'admin/store/settings/charges/per_order';
    } else {
        drupal_write_record('uc_charges_per_order', $form_state['values']);
        entity_flush_caches();
        drupal_set_message(t('Created and enabled new per order charges.'));
        $form_state['redirect'] = 'admin/store/settings/charges/per_order';
    }
}

/**
 * Helper function to delete a per_order_charges method.
 *
 * @see uc_per_order_charges_admin_method_edit_form()
 */
function uc_charges_per_order_admin_method_edit_form_delete($form, &$form_state)
{
    drupal_goto('admin/store/settings/charges/per_order_charges/' . $form_state['values']['mid'] . '/delete');
}

/**
 * Confirms deletion of Per Order Charges.
 *
 * @see uc_per_order_charges_admin_method_confirm_delete_submit()
 * @ingroup forms
 */
function uc_charges_per_order_admin_method_confirm_delete($form, &$form_state, $mid)
{
    $form['mid'] = array('#type' => 'value', '#value' => $mid);
    return confirm_form($form, t('Do you want to delete this per order charges?'),
        'admin/store/settings/charges/per_order_charges',
        t('This will remove the per order charges.'),
        t('Delete'));
}

/**
 * Form submission handler for uc_per_order_charges_admin_method_confirm_delete().
 *
 * @see uc_per_order_charges_admin_method_confirm_delete()
 */
function uc_charges_per_order_admin_method_confirm_delete_submit($form, &$form_state)
{
    $mid = $form_state['values']['mid'];
    db_delete('uc_charges_per_order')
        ->condition('mid', $mid)
        ->execute();
    rules_config_delete(array('get_quote_from_per_order_charges_' . $mid));
    drupal_set_message(t('Per Order Charges deleted.'));
    $form_state['redirect'] = 'admin/store/settings/charges/per_order';
}

/**
 * Confirms disable of per order charges.
 *
 * @see uc_per_order_charges_admin_method_confirm_disable_submit()
 * @ingroup forms
 */
function uc_charges_per_order_admin_method_confirm_disable($form, &$form_state, $mid = 0)
{
    $active = 0;
    $form['mid'] = array('#type' => 'value', '#value' => $mid);
    $form['active'] = array('#type' => 'value', '#value' => $active);
    return confirm_form($form, t('Do you want to Disable this per order charges?'),
        'admin/store/settings/charges/per_order',
        t('This per order charges will be disable'),
        t('Confirm'));
}

/**
 * Form submission handler for uc_per_order_charges_admin_method_confirm_disable().
 *
 * @see uc_per_order_charges_admin_method_confirm_disable()
 */
function uc_charges_per_order_admin_method_confirm_disable_submit($form, &$form_state)
{
    drupal_write_record('uc_charges_per_order', $form_state['values'], 'mid');
    drupal_set_message(t('Disabled per order charges.'));
    $form_state['redirect'] = 'admin/store/settings/charges/per_order';
}

/**
 * Confirms enable of per order charges.
 *
 * @see uc_per_order_charges_admin_method_confirm_enable_submit()
 * @ingroup forms
 */
function uc_charges_per_order_admin_method_confirm_enable($form, &$form_state, $mid = 0)
{
    $active = 1;
    $form['mid'] = array('#type' => 'value', '#value' => $mid);
    $form['active'] = array('#type' => 'value', '#value' => $active);
    return confirm_form($form, t('Do you want to Enable this per order charges?'),
        'admin/store/settings/charges/per_order',
        t('This per order charges will be enable'),
        t('Confirm'));
}

/**
 * Form submission handler for uc_per_order_charges_admin_method_confirm_enable().
 *
 * @see uc_per_order_charges_admin_method_confirm_enable()
 */
function uc_charges_per_order_admin_method_confirm_enable_submit($form, &$form_state)
{
    drupal_write_record('uc_charges_per_order', $form_state['values'], 'mid');
    drupal_set_message(t('Enabled per order charges.'));
    $form_state['redirect'] = 'admin/store/settings/charges/per_order';
}

/**
 * Displays a formatted list of Per Item Charges a_charge methods and form elements.
 *
 * @see uc_per_item_charges_method_settings()
 * @ingroup themeable
 */
function theme_uc_charges_per_item_method_settings($variables)
{
    $form = $variables['form'];
    drupal_add_tabledrag('uc-charges_per_item-methods', 'order', 'sibling', 'uc-charges-method-weight');
    $header = array(t('Name of per item charges'), t('Charges'),
        t('Operations'));
    $rows = array();
    foreach (element_children($form['methods']) as $method) {
        $row = array(
            drupal_render($form['methods'][$method]['uc_charges_per_item_enabled']),
            drupal_render($form['methods'][$method]['description']),
            drupal_render($form['methods'][$method]['operations']),
        );
        $rows[] = array(
            'data' => $row,
            'class' => array('draggable'),
        );
    }
    $output = theme('table', array(
        'header' => $header,
        'rows' => $rows,
        'attributes' => array('id' => 'uc-charges_per_item-methods'),
        'empty' => t('No per item charges have been configured yet.'),
    ));
    $output .= drupal_render_children($form);
    return $output;
}

/**
 * Displays a formatted list of Per Order Charges a_charge methods and form elements.
 * @see uc_per_order_charges_method_settings()
 * @ingroup themeable
 */
function theme_uc_charges_per_order_method_settings($variables)
{
    $form = $variables['form'];
    drupal_add_tabledrag('uc-charges_per_order-methods', 'order', 'sibling', 'uc-charges-method-weight');
    $header = array(t('Name of per order charges'), t('Charges'),
        t('Operations'));
    $rows = array();
    foreach (element_children($form['methods']) as $method) {
        $row = array(
            drupal_render($form['methods'][$method]['uc_charges_per_order_enabled']),
            drupal_render($form['methods'][$method]['description']),
            drupal_render($form['methods'][$method]['operations']),
        );
        $rows[] = array(
            'data' => $row,
            'class' => array('draggable'),
        );
    }
    $output = theme('table', array(
        'header' => $header,
        'rows' => $rows,
        'attributes' => array('id' => 'uc-charges_per_order-methods'),
        'empty' => t('No per order charges have been configured yet.'),
    ));
    $output .= drupal_render_children($form);
    return $output;
}

/**
 * Form validation for uc_per_item_method_settings().
 *
 * Requires at least one enabled charges method.
 *
 * @see uc_charges_method_settings()
 * @see uc_charges_method_settings_submit()
 */
function uc_charges_per_item_method_settings_validate($form, &$form_state)
{
    $none_enabled = TRUE;
    if (is_array($form_state['values']['methods'])) {
        foreach ($form_state['values']['methods'] as $method) {
            if ($method['uc_per_item_charges_enabled']) {
                $none_enabled = FALSE;
            }
        }
    }
    if ($none_enabled) {
        form_set_error('uc_charges_per_item_enabled', t('At least one per_item_charges method must be enabled.'));
    }
}

/**
 * Form submission handler for uc_per_item_charges_method_settings().
 *
 * @see uc_charges_method_settings()
 * @see uc_charges_method_settings_validate()
 */
function uc_charges_per_item_method_settings_submit($form, &$form_state)
{
    $enabled = array();
    $method_weight = array();
    foreach ($form_state['values']['methods'] as $id => $method) {
        $enabled[$id] = $method['uc_charges_per_item_enabled'];
        $method_weight[$id] = $method['uc_charges_per_item_method_weight'];
    }
    variable_set('uc_charges_per_item_enabled', $enabled);
    variable_set('uc_charges_per_item_method_weight', $method_weight);
    drupal_set_message(t('The configuration options have been saved.'));
}
