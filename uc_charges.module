<?php
/**
 * Copyright 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package uc_charges
 * @author Horoppa Software
 * @link http://www.horoppa.com
 * @since Version 1.0
 */

/**
 * Implements hook permission().
 */
function uc_charges_permission()
{
    return array(
        'configure a_charges' => array(
            'title' => t('configure a_charges'),
        ),
    );
}

/**
 * Implements hook menu().
 */
function uc_charges_menu()
{
    $items = array();
    $items['admin/store/settings/charges'] = array(
        'title' => 'Charges',
        'description' => 'Configure Charges.',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('uc_charges_per_item_method_settings'),
        'access arguments' => array('configure a_charges'),
        'file' => 'uc_charges.admin.inc',
    );
    $items['admin/store/settings/charges/add'] = array(
        'title' => 'Per item charges',
        'description' => 'Create a per item charges.',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('uc_charges_per_item_method_settings'),
        'access arguments' => array('configure a_charges'),
        'type' => MENU_DEFAULT_LOCAL_TASK,
        'file' => 'uc_charges.admin.inc',
    );
    $items['admin/store/settings/charges/per_order'] = array(
        'title' => 'Per order charges',
        'description' => 'Create per order charges.',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('uc_charges_per_order_method_settings'),
        'access arguments' => array('configure a_charges'),
        'type' => MENU_LOCAL_TASK,
        'file' => 'uc_charges.admin.inc',
    );
    $items['admin/store/settings/charges/per_item_add'] = array(
        'title' => 'Per item charges',
        'description' => 'Create a per item charges.',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('uc_charges_per_item_admin_method_edit_form'),
        'access arguments' => array('configure a_charges'),
        'type' => MENU_LOCAL_ACTION,
        'file' => 'uc_charges.admin.inc',
    );
    $items['admin/store/settings/charges/per_order_add'] = array(
        'title' => 'Per order charges',
        'description' => 'Create per order charges.',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('uc_charges_per_order_admin_method_edit_form'),
        'access arguments' => array('configure a_charges'),
        'type' => MENU_LOCAL_ACTION,
        'file' => 'uc_charges.admin.inc',
    );
    $items['admin/store/settings/charges/methods/per_item_charges/%'] = array(
        'title' => 'Edit per item charges',
        'description' => 'Edit an existing per item charges.',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('uc_charges_per_item_admin_method_edit_form', 6),
        'access arguments' => array('configure a_charges'),
        'file' => 'uc_charges.admin.inc',
    );
    $items['admin/store/settings/charges/charges/%/delete'] = array(
        'title' => 'Delete per item charges',
        'description' => 'Delete per item charges.',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('uc_charges_per_item_admin_method_confirm_delete', 5),
        'access arguments' => array('configure a_charges'),
        'file' => 'uc_charges.admin.inc',
    );
    $items['admin/store/settings/charges/methods/per_order_charges/%'] = array(
        'title' => 'Edit per order charges',
        'description' => 'Edit an existing per order charges.',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('uc_charges_per_order_admin_method_edit_form', 6),
        'access arguments' => array('configure a_charges'),
        'file' => 'uc_charges.admin.inc',
    );
    $items['admin/store/settings/charges/per_order_charges/%/delete'] = array(
        'title' => 'Delete per order charges',
        'description' => 'Delete a per order charges.',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('uc_charges_per_order_admin_method_confirm_delete', 5),
        'access arguments' => array('configure a_charges'),
        'file' => 'uc_charges.admin.inc',
    );
    $items['admin/store/settings/charges/per_order_charges/%/disable'] = array(
        'title' => 'Disable per order charges',
        'description' => 'Disable per order charges.',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('uc_charges_per_order_admin_method_confirm_disable', 5),
        'access arguments' => array('configure a_charges'),
        'file' => 'uc_charges.admin.inc',
    );
    $items['admin/store/settings/charges/per_order_charges/%/enable'] = array(
        'title' => 'Disable per order charges',
        'description' => 'Disable per order charges.',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('uc_charges_per_order_admin_method_confirm_enable', 5),
        'access arguments' => array('configure a_charges'),
        'file' => 'uc_charges.admin.inc',
    );

    return $items;
}

/**
 * Implements hook per_item_method().
 */
function uc_charges_per_item_method()
{
    $methods = array();
    $result = db_query("SELECT mid, title, label, base_rate FROM {uc_charges_per_item}");
    foreach ($result as $method) {
        $methods['charges'] = array(
            'id' => 'charges_' . $method->mid,
            'module' => 'uc_charges',
            'title' => $method->title,
            'description' => t('!base_rate', array('!base_rate' => uc_currency_format($method->base_rate))),
            'operations' => array(
                'edit' => array(
                    'title' => t('edit'),
                    'href' => 'admin/store/settings/charges/methods/per_item_charges/' . $method->mid,
                ),
                'delete' => array(
                    'title' => t('delete'),
                    'href' => 'admin/store/settings/charges/charges/' . $method->mid . '/delete',
                ),
            ),
            'a_charge' => array(
                'type' => 'order',
                'callback' => 'uc_charges_a_charge_per_item',
                'accessorials' => array(
                    $method->label,
                    $method->base_rate
                ),
            ),
            'enabled' => TRUE,
        );
    }

    return $methods;
}

/**
 * Implements hook per_order_method().
 */
function uc_charges_per_order_method()
{
    $methods = array();
    $result = db_query("SELECT mid, title, label, base_rate,active FROM {uc_charges_per_order} where active=1");
    foreach ($result as $method) {
        $methods['charges_' . $method->mid] = array(
            'id' => 'charges_' . $method->mid,
            'module' => 'uc_charges',
            'title' => $method->title,
            'description' => t('!base_rate', array('!base_rate' => uc_currency_format($method->base_rate))),
            'operations' => array(
                'edit' => array(
                    'title' => t('edit'),
                    'href' => 'admin/store/settings/charges/methods/per_order_charges/' . $method->mid,
                ),
                'delete' => array(
                    'title' => t('delete'),
                    'href' => 'admin/store/settings/charges/per_order_charges/' . $method->mid . '/delete',
                ),
                'Disable' => array(
                    'title' => t('disable'),
                    'href' => 'admin/store/settings/charges/per_order_charges/' . $method->mid . '/disable',
                ),
            ),
            'a_charge' => array(
                'type' => 'order',
                'callback' => 'uc_charges_a_charge',
                'accessorials' => array(
                    $method->label,
                    $method->mid
                ),
            ),
            'enabled' => TRUE,
        );
    }
    $result = db_query("SELECT mid, title, label, base_rate,active FROM {uc_charges_per_order} where active=0");
    foreach ($result as $method) {
        $methods['charges_' . $method->mid] = array(
            'id' => 'charges_' . $method->mid,
            'module' => 'uc_charges',
            'title' => $method->title,
            'description' => t('!base_rate', array('!base_rate' => uc_currency_format($method->base_rate))),
            'operations' => array(
                'edit' => array(
                    'title' => t('edit'),
                    'href' => 'admin/store/settings/charges/methods/per_order_charges/' . $method->mid,
                ),
                'delete' => array(
                    'title' => t('delete'),
                    'href' => 'admin/store/settings/charges/per_order_charges/' . $method->mid . '/delete',
                ),
                'Enable' => array(
                    'title' => t('enable'),
                    'href' => 'admin/store/settings/charges/per_order_charges/' . $method->mid . '/enable',
                ),
            ),
            'a_charge' => array(
                'type' => 'order',
                'callback' => 'uc_charges_a_charge',
                'accessorials' => array(
                    $method->label,
                    $method->mid
                ),
            ),
            'enabled' => TRUE,
        );
    }
    return $methods;
}

/**
 * Implements uc_charges_per_order_active_method().
 */
function uc_charges_per_order_active_method()
{
    $methods = array();
    $result = db_query("SELECT mid, title, label, base_rate,active FROM {uc_charges_per_order} where active=1");
    foreach ($result as $method) {
        $methods['charges_' . $method->mid] = array(
            'id' => 'charges_' . $method->mid,
            'module' => 'uc_charges',
            'title' => $method->title,
            'description' => t('!base_rate', array('!base_rate' => uc_currency_format($method->base_rate))),
            'operations' => array(
                'edit' => array(
                    'title' => t('edit'),
                    'href' => 'admin/store/settings/charges/methods/per_order_charges/' . $method->mid,
                ),
                'delete' => array(
                    'title' => t('delete'),
                    'href' => 'admin/store/settings/charges/per_order_charges/' . $method->mid . '/delete',
                ),
                'Disable' => array(
                    'title' => t('disable'),
                    'href' => 'admin/store/settings/charges/per_order_charges/' . $method->mid . '/disable',
                ),
            ),
            'a_charge' => array(
                'type' => 'order',
                'callback' => 'uc_charges_a_charge',
                'accessorials' => array(
                    $method->label,
                    $method->mid
                ),
            ),
            'enabled' => TRUE,
        );
    }

    return $methods;
}

/**
 * Implements uc_charges_per_item_methods().
 */
function uc_charges_per_item_methods($all = FALSE)
{
    $enabled = variable_get('uc_charges_per_item_enabled', array());
    $weight = variable_get('uc_charges_per_item_method_weight', array());
    $methods = array();
    foreach (uc_charges_per_item_method() as $id => $method) {

        // Set defaults.
        $method += array(
            'enabled' => FALSE,
            'weight' => 0,
        );

        // Override defaults with store configuration, if any.
        if (isset($enabled[$id])) {
            $method['enabled'] = $enabled[$id];

        }
        if (isset($weight[$id])) {
            $method['weight'] = $weight[$id];
        }
        if ($all || $method['enabled']) {
            $methods[$id] = $method;
        }
    }

    return $methods;
}

/**
 * Implements uc_charges_per_order_methods().
 */
function uc_charges_per_order_methods($all = FALSE)
{
    $enabled = variable_get('uc_charges_per_order_enabled', array());
    $weight = variable_get('uc_charges_per_order_method_weight', array());
    $methods = array();
    foreach (uc_charges_per_order_method() as $id => $method) {

        // Set defaults.
        $method += array(
            'enabled' => FALSE,
            'weight' => 0,
        );

        // Override defaults with store configuration, if any.
        if (isset($enabled[$id])) {
            $method['enabled'] = $enabled[$id];
        }
        if (isset($weight[$id])) {
            $method['weight'] = $weight[$id];
        }
        if ($all || $method['enabled']) {
            $methods[$id] = $method;
        }
    }

    return $methods;
}

/**
 * Implements uc_charges_per_order_active_methods().
 */
function uc_charges_per_order_active_methods($all = FALSE)
{
    $enabled = variable_get('uc_charges_per_order_enabled', array());
    $weight = variable_get('uc_charges_per_order_method_weight', array());
    $methods = array();
    foreach (uc_charges_per_order_active_method() as $id => $method) {

        // Set defaults.
        $method += array(
            'enabled' => FALSE,
            'weight' => 0,
        );

        // Override defaults with store configuration, if any.
        if (isset($enabled[$id])) {
            $method['enabled'] = $enabled[$id];
        }
        if (isset($weight[$id])) {
            $method['weight'] = $weight[$id];
        }
        if ($all || $method['enabled']) {
            $methods[$id] = $method;
        }
    }

    return $methods;
}

/**
 * Implements hook theme().
 */
function uc_charges_theme()
{
    return array(
        'uc_charges_per_item_method_settings' => array(
            'render element' => 'form',
            'file' => 'uc_charges.admin.inc',
        ),
        'uc_charges_per_order_method_settings' => array(
            'render element' => 'form',
            'file' => 'uc_charges.admin.inc',
        ),
    );
}

/**
 * Standard callback to charges.
 *
 * @return
 *   An array containing the per order charges a_charge for the order.
 */
function uc_charges_a_charge($products, $details, $method)
{
    $method = explode('_', $method['id']);
    $mid = $method[1];
    if ($method = db_query("SELECT * FROM {uc_charges_per_order} WHERE mid = :mid", array(':mid' => $mid))->fetchObject()) {
        $rate = $method->base_rate;
        $a_charges[] = array(
            'rate' => $rate,
            'label' => check_plain($method->mid),
            'option_label' => check_plain($method->label),
        );
    }

    return $a_charges;
}

/**
 * Standard callback to charges.
 *
 * @return
 *   An array containing the per item charges a_charge for the order.
 */
function uc_charges_a_charge_per_item($products, $details, $method)
{
    $method = explode('_', $method['id']);
    $mid = $method[1];
    if ($method = db_query("SELECT * FROM {uc_charges_per_item} WHERE mid = :mid", array(':mid' => $mid))->fetchObject()) {
        $rate = $method->base_rate;
        $title = $method->title;
        $charge_type = 'per_item_charges';

        $a_charges[] = array(
            'rate' => $rate,
            'title' => $title,
            'charge_type' => $charge_type,
            'mid' => $mid
        );
    }

    return $a_charges;
}

/**
 * Defines the per order and per item charges checkout pane.
 */
function uc_charges_uc_checkout_pane()
{
    $panes['charge'] = array(
        'callback' => 'uc_checkout_pane_charge_per_order',
        'title' => t('Charges(Per order)'),
        'desc' => t('Extra information necessary to ship.'),
        'weight' => 5,
        'shippable' => TRUE,
    );
    $panes['charge2'] = array(
        'callback' => 'uc_checkout_pane_charge_per_item',
        'title' => t('Charges(Per Item)'),
        'desc' => t('Extra information necessary to ship.'),
        'weight' => 5,
        'shippable' => TRUE,
    );

    return $panes;
}

/**
 * Per order charges checkout pane callback.
 *
 * Adds a line item to the order that records the chosen per order charges.
 *
 * @see uc_per_order_a_charge_checkout_pane_a_charges_submit()
 * @see uc_per_order_a_charge_checkout_returned_rates()
 */
function uc_checkout_pane_charge_per_order($op, &$order, $form = NULL, &$form_state = NULL)
{
    global $user;
    switch ($op) {
        case 'view':
            $check_order_charge = $order->a_charge_form;
            if (!empty($check_order_charge)) {
                $contents['#attached']['css'][] = drupal_get_path('module', 'uc_charges') . '/uc_charges.css';
                $contents['page'] = array(
                    '#type' => 'hidden',
                    '#value' => 'checkout',
                );
                $contents['uid'] = array(
                    '#type' => 'hidden',
                    '#value' => $user->uid,
                );
                $ul_html = '<ul style="list-style-type: square;">';
                foreach ($order->a_charge_form['a_charge_option']['#options'] as $item) {
                    $ul_html .= "<li class='per_order_list'>$item</li>";
                }
                $ul_html .= "</ul>";
                $contents['a_charges'] = array(
                    '#tree' => TRUE,
                    '#prefix' => '<div id="a_charge" class="per_order_area">',
                    '#suffix' => $ul_html . '</div>',
                    '#weight' => 1,
                );
                return array('contents' => $contents);
            }

        case 'prepare':
        case 'process':
            $order->a_charge_form = uc_charges_build_per_order_form($order, !empty($form_state['a_charge_requested']));
            if ($order->a_charge_form) {
                $result = db_query("SELECT line_item_id FROM {uc_order_line_items} WHERE order_id = :id AND type = :type", array(':id' => $order->order_id, ':type' => 'per_order_charges'));
                $a = $order->a_charge_form['a_charge_option']['#a'];
                foreach ($a as $row) {
                    foreach ($row as $row1) {
                        $label = $row1['option_label'];
                        $order->a_charge['rate'] = $row1['rate'];
                        if ($lid = $result->fetchField()) {
                            uc_order_update_line_item($lid,
                                $label,
                                $order->a_charge['rate']
                            );
                        } else {
                            uc_order_line_item_add($order->order_id, 'per_order_charges',
                                $label,
                                $order->a_charge['rate']
                            );
                        }
                    }
                }

            } else {
                unset($order->a_charge);
            }
            if (!isset($order->a_charge) && $op == 'process' && variable_get('uc_a_charge_require_a_charge', TRUE)) {
                form_set_error('panes][a_charges][a_charges][a_charge_option', t('You must select a charges option before continuing.'));
                return FALSE;
            } else {
                return TRUE;
            }

        case 'review':
            $review = array();
            $result = db_query("SELECT * FROM {uc_order_line_items} WHERE order_id = :id AND type = :type", array(':id' => $order->order_id, ':type' => 'per_order_charges'));
            foreach($result as $method) {
                $review[] = array('title' =>$method->title, 'data' => theme('uc_price', array('price' => $method->amount)));
            }
            return $review;
    }
}

/**
 * Per item charges checkout pane callback.
 *
 * Adds a line item to the order that records the chosen per item charges.
 *
 * @see uc_per_item_a_charge_checkout_pane_a_charges_submit()
 * @see uc_per_item_a_charge_checkout_returned_rates()
 */
function uc_checkout_pane_charge_per_item($op, &$order, $form = NULL, &$form_state = NULL)
{
    global $user;

    switch ($op) {
        case 'view':
            $check_item_charge = $order->a_item_form['a_charge_option']['#a'];
            if (!empty($check_item_charge)) {
                $contents['#attached']['css'][] = drupal_get_path('module', 'uc_charges') . '/uc_charges.css';
                $contents['page'] = array(
                    '#type' => 'hidden',
                    '#value' => 'checkout',
                );
                $contents['uid'] = array(
                    '#type' => 'hidden',
                    '#value' => $user->uid,
                );
                $contents['a_charges'] = array(
                    '#tree' => TRUE,
                    '#prefix' => '<div id="a_charge" class="per_item_area">',
                    '#suffix' => $order->label . ' : ' . number_format($order->rate, 2) . ' X ' . $order->qty . ' = $' . $order->total . '</div>',
                    '#weight' => 1,
                );
                $contents['a_charges'] += $order->a_item_form;
                return array('contents' => $contents);
            }

        case 'prepare':
        case 'process':
            $order->a_item_form = uc_charges_build_per_item_form($order, !empty($form_state['a_charge_requested']));
            $cart_id = $order->uid;
            $b = $order->a_item_form['a_charge_option']['#a'];
            $order->a_charge['rate'] = $order->a_item_form['a_charge_option']['#a'];
            $method = db_query("SELECT sum(qty) as qty FROM {uc_cart_products} WHERE cart_id = :cart_id", array(':cart_id' => $cart_id))->fetchObject();
            if ($order->a_item_form) {
                $result = db_query("SELECT line_item_id FROM {uc_order_line_items} WHERE order_id = :id AND type = :type", array(':id' => $order->order_id, ':type' => 'per_item_charges'));
                $total_qty_order = $method->qty;
                $order->qty = $total_qty_order;
                foreach ($b as $row) {
                    foreach ($row as $row1) {
                        $label = $row1['title'];
                        $order->label = $label;
                        $order->a_charge['rate'] = $row1['rate'] * ($total_qty_order);
                        $order->total = $order->a_charge['rate'];
                        $order->rate = $row1['rate'];
                        if ($lid = $result->fetchField()) {
                            uc_order_update_line_item($lid,
                                $label,
                                $order->a_charge['rate']
                            );
                        } else {
                            uc_order_line_item_add($order->order_id, 'per_item_charges',
                                $label,
                                $order->a_charge['rate']
                            );
                        }
                    }
                }
            } else {
                unset($order->a_charge);
            }

            if (!isset($order->a_charge) && $op == 'process' && variable_get('uc_a_charge_require_a_charge', TRUE)) {
                form_set_error('panes][a_charges][a_charges][a_charge_option', t('You must select a charges option before continuing.'));
                return FALSE;
            } else {
                return TRUE;
            }

        case 'review':
            $review = array();
            $result = db_query("SELECT * FROM {uc_order_line_items} WHERE order_id = :id AND type = :type", array(':id' => $order->order_id, ':type' => 'per_item_charges'));
            if ($line_item = $result->fetchAssoc()) {
                $review[] = array('title' => $line_item['title'], 'data' => theme('uc_price', array('price' => $line_item['amount'])));
            }
            return $review;
    }
}

/**
 * Calculates and returns the per order charges selection form.
 */
function uc_charges_build_per_order_form($order, $show_errors = TRUE)
{
    $return = array();
    $a_charges = uc_charges_assemble_a_charges_per_order($order);
    $a_charge_options = array();
    if (!empty($a_charges)) {
        foreach ($a_charges as $method => $data) {
            foreach ($data as $accessorial => $a_charge) {
                $key = $method . '---' . $accessorial;

                if (isset($a_charge['rate'])) {
                    $a_charge_options[$key] = t('!label: !price', array('!label' => $a_charge['option_label'], '!price' => $a_charge['format']));
                    $return[$key]['rate'] = array(
                        '#type' => 'hidden',
                        '#type' => 'option_label',
                        '#value' => $a_charge['rate'],
                    );
                }
                if (!empty($a_charge['error'])) {
                    $return[$key]['error'] = array(
                        '#markup' => '<div cla_charge="a_charge-error">' . theme('item_list', array('items' => $a_charge['error'])) . '</div>',
                    );
                }
                if (!empty($a_charge['notes'])) {
                    $return[$key]['notes'] = array(
                        '#markup' => '<div cla_charge="a_charge-notes">' . $a_charge['notes'] . '</div>',
                    );
                }
                if (!empty($a_charge['debug'])) {
                    $return[$key]['debug'] = array(
                        '#markup' => '<pre>' . $a_charge['debug'] . '</pre>',
                    );
                }
                if (!isset($a_charge['rate']) && isset($a_charge['label']) && count($return[$key])) {
                    $return[$key]['#prefix'] = $a_charge['label'] . ': ';
                }
            }
        }
    }

    $num_a_charges = count($a_charge_options);
    if ($num_a_charges >= 1) {
        $return['a_charge_option'] = array(
            '#type' => 'hidden',
            '#num_a_charges' => $num_a_charges,
            '#options' => $a_charge_options,
            '#a' => $a_charges
        );
    } elseif ($show_errors) {
        $return['error'] = array(
            '#markup' => filter_xss_admin(variable_get('uc_a_charge_err_msg',
            t("There were problems getting a shipping a_charge. Please verify the delivery address and product information and try again.\nIf this does not resolve the issue,
            please call @phone to complete your order.",
            array('@phone' => variable_get('uc_store_phone', NULL))))),
        );
    }

    return $return;
}

/**
 * Calculates and returns the per item charges selection form.
 */
function uc_charges_build_per_item_form($order, $show_errors = TRUE)
{
    $return = array();
    $a_charges = uc_charges_assemble_a_charges_per_item($order);
    $a_charge_options = array();
    if (!empty($a_charges)) {
        foreach ($a_charges as $method => $data) {
            foreach ($data as $accessorial => $a_charge) {
                $key = $method . '---' . $accessorial;
                if (!empty($a_charge['error'])) {
                    $return[$key]['error'] = array(
                        '#markup' => '<div cla_charge="a_charge-error">' . theme('item_list', array('items' => $a_charge['error'])) . '</div>',
                    );
                }
                if (!empty($a_charge['notes'])) {
                    $return[$key]['notes'] = array(
                        '#markup' => '<div cla_charge="a_charge-notes">' . $a_charge['notes'] . '</div>',
                    );
                }
                if (!empty($a_charge['debug'])) {
                    $return[$key]['debug'] = array(
                        '#markup' => '<pre>' . $a_charge['debug'] . '</pre>',
                    );
                }
                if (!isset($a_charge['rate']) && isset($a_charge['label']) && count($return[$key])) {
                    $return[$key]['#prefix'] = $a_charge['label'] . ': ';
                }
            }
        }
    }
    $return['a_charge_option'] = array(
        '#type' => 'radios',
        '#options' => $a_charge_options,
        '#a' => $a_charges,
    );

    return $return;
}

/**
 * Form submission handler for uc_checkout_pane_a_charges().
 *
 * @see uc_checkout_pane_a_charges()
 */
function uc_charges_checkout_pane_a_charges_submit($form, &$form_state)
{
    $form_state['rebuild'] = TRUE;
    $form_state['a_charge_requested'] = TRUE;
}

/**
 * Pulls the get_a_charge_per_order_from_* triggers and assemble_a_charges their returned data.
 */
function uc_charges_assemble_a_charges_per_order($order)
{
    global $user;
    if (!$order->uid || !($account = user_load($order->uid))) {
        $account = $user;
    }
    $methods = uc_charges_per_order_active_methods();
    $a_charge_data = array();
    foreach ($methods as $method) {
        $set = rules_config_load('get_charges_from_' . $method['id']);
        if (!$set || $set->execute($order)) {
            $data = uc_charges_action_get_a_charge($order, $method);
            foreach ($data as &$a_charge) {
                if (isset($a_charge['rate'])) {
                    $a_charge['format'] = uc_currency_format($a_charge['rate']);
                }
            }
            $a_charge_data[$method['id']] = $data;
        }
    }

    return $a_charge_data;
}

/**
 * Pulls the get_a_charge_from_per_item_* triggers and assemble_a_charges their returned data.
 */
function uc_charges_assemble_a_charges_per_item($order)
{
    global $user;
    if (!$order->uid || !($account = user_load($order->uid))) {
        $account = $user;
    }
    $methods = uc_charges_per_item_methods();
    $a_charge_data = array();
    foreach ($methods as $method) {
        $set = rules_config_load('get_charges_from_' . $method['id']);
        if (!$set || $set->execute($order)) {
            $data = uc_charges_action_get_a_charge($order, $method);
            foreach ($data as &$a_charge) {
                if (isset($a_charge['rate'])) {
                    $a_charge['format'] = uc_currency_format($a_charge['rate']);
                }
            }
            $a_charge_data[$method['id']] = $data;
        }
    }

    return $a_charge_data;
}

/**
 * Action handler for a given charge.
 */
function uc_charges_action_get_a_charge($order, $method)
{
    $details = array();
    foreach ($order as $key => $value) {
        if (substr($key, 0, 9) == 'delivery_') {
            $field = substr($key, 9);
            $details[$field] = $value;
        }
    }
    ob_start();

    // Load include file containing a_charge callback, if there is one
    if (isset($method['a_charge']['file'])) {
        $inc_file = drupal_get_path('module', $method['module']) . '/' . $method['a_charge']['file'];
        if (is_file($inc_file)) {
            require_once($inc_file);
        }
    }
    if (function_exists($method['a_charge']['callback'])) {
        $products = array();
        $a_charge_data = call_user_func($method['a_charge']['callback'], $products, $details, $method);
    }
    $messages = ob_get_contents();
    ob_end_clean();

    if ($messages && variable_get('uc_charges_log_errors', FALSE)) {
        watchdog('charges', '!messages', array('!messages' => $messages), WATCHDOG_WARNING);
        watchdog('charges', '<pre>@data</pre>', array('@data' => print_r($a_charge_data, TRUE)), WATCHDOG_WARNING);
    }

    return $a_charge_data;
}

/**
 * implements hook uc_line_item.
 */
function uc_charges_uc_line_item()
{
    $items['per_order_charges'] = array(
        'title' => t('Charges'),
        'weight' => 1,
        'default' => FALSE,
        'stored' => TRUE,
        'calculated' => TRUE,
        'display_only' => FALSE,
        'add_list' => TRUE,
    );
    $items['per_item_charges'] = array(
        'title' => t('Charges'),
        'weight' => 1,
        'default' => FALSE,
        'stored' => TRUE,
        'calculated' => TRUE,
        'display_only' => FALSE,
        'add_list' => TRUE,
    );

    return $items;
}
